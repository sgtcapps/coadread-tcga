# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('#outcome_summary').dataTable(
    sPaginationType: "full_numbers"
    bJQueryUI: true
    aaSorting: [[0, "asc" ]]
    iDisplayLength: 25
    aLengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]]
    aoColumns: [
            { sWidth: '5%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '22%' },
            { sWidth: '8%' }]
   )
   
jQuery ->   
  $('#outcome_summary_all').dataTable(
    sPaginationType: "full_numbers"
    bJQueryUI: true
    aaSorting: [[1, "asc" ]]
    iDisplayLength: 25
    aLengthMenu: [[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]]
    aoColumns: [
            { sWidth: '10%' },
            { sWidth: '5%' },
            { sWidth: '9%' },
            { sWidth: '6%' },
            { sWidth: '8%' },
            { sWidth: '7%' },
            { sWidth: '8%' },
            { sWidth: '9%' },
            { sWidth: '8%' },
            { sWidth: '22%' },
            { sWidth: '8%' }]
   )

jQuery ->   
  oTable = $('#outcome_summary_input').dataTable(
    sPaginationType: "full_numbers"
    bJQueryUI: true
    aaSorting: [[1, "asc" ]]
    iDisplayLength: 25
    aLengthMenu: [[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]]
    bFilter: true
    aoColumns: [
            { sWidth: '10%' },
            { sWidth: '5%' },
            { sWidth: '9%' },
            { sWidth: '6%' },
            { sWidth: '8%' },
            { sWidth: '7%' },
            { sWidth: '8%' },
            { sWidth: '9%' },
            { sWidth: '8%' },
            { sWidth: '22%' },
            { sWidth: '8%' }]
   )
  oTable.fnFilter(gon.search_input, 2, true, false, true) if gon? #passing variables to be filtered in datatables output
   


