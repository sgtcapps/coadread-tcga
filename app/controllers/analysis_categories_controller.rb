class AnalysisCategoriesController < ApplicationController
  
  # GET /analysis_results
  # GET /analysis_results.json
  def index
    respond_to do |format|
       format.html # index.html.erb
       format.json { render json: @analysis_results }
     end
  end
  
  def select_categories
    cancer_type    = 'COADREAD'
    data_set       = '2013-01-16'
    gene_set       = 'CancerGenes'
    mir_set        = 'AllmiR'
    hi_lev_genomic = 'Integrative'

    summary_table_array = HighLevelSummary.get_summary_table(cancer_type, data_set, gene_set, hi_lev_genomic)
    summary_table_array.shift(2) #remove the header lines
    render 'select_categories', :locals => {summary_table_array: summary_table_array, cancer_type: cancer_type, data_set: data_set, gene_set: gene_set, mir_set: mir_set, hi_lev_genomic: hi_lev_genomic}
  end
  
  def get_feature_samples
    @filename = sanitize_file_name(params[:filename])
    @metadata_filename = @filename.gsub(/\A.*_([\w-]+\.txt)\Z/, '\1' ) if !@filename.blank? #get just the feature part of filename at end

    @table_array = AnalysisCategory.get_feature_samples_table(@filename) if !@filename.blank?
    @table_array_metadata = AnalysisCategory.get_feature_samples_metadata_table(@metadata_filename)
 
    @hash_metadata = @table_array_metadata.inject(Hash.new{ |h,k| h[k]=[] }){ |h,(k,v)| h[k] << v; h } if !@table_array_metadata.blank?
      
    if @table_array.blank? || @hash_metadata.blank?
      respond_to do |format|
        format.html { 
            flash[:notice] = "Sorry, no sample data was found for #{@metadata_filename.gsub('.txt','')}"         
            redirect_to root_path
          }
        end # /respond_to
    else
      @metadata_headings = @table_array_metadata.shift;
      render 'feature_samples_table'
    end

  end
  
  def send_feature_samples_file(filename = sanitize_file_name(params[:filename]))
    table_array = AnalysisCategory.get_feature_samples_table(filename)
    csv_file = ApplicationController.to_csv(filename, table_array)
    send_data csv_file, :type => 'text/csv; charset=iso-8859-1; header=present', 
      :disposition => "attachment;filename=#{filename.gsub('.txt', '.csv')}"
  end

end
