class ApplicationController < ActionController::Base
  protect_from_forgery

  protected
  def sanitize_file_name(filename)
    file_name = File.basename(filename) #Remove any leading relative directory info (hacking protection)
  end

  def self.get_table_array(path)
    require 'csv'
    table_array = CSV.read(path, {:col_sep => "\t"})
    table_array
  end
  
  def self.to_csv(file_name, array)
    require 'csv'    
    file = CSV.generate do |csv|
        #csv << header if not header.blank?
        csv << array.shift
        array.map {|row| csv << row}
    end
    file
  end  
  
end
