class OutcomeSummariesController < ApplicationController
  # GET /outcome_summaries
  # GET /outcome_summaries.json
  def index
    #@outcome_summaries = OutcomeSummary.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @outcome_summaries }
    end
  end

  def show_outcome_table
           
    cancer_type = params[:cancer_type]
    data_set = params[:data_set]
    gene_set = params[:gene_set]
    mir_set  = params[:mir_set]
    cosmic_gene_set = params[:cosmic_gene_set]
    genomic = params[:genomic]
    hi_lev_genomic = params[:hi_lev_genomic]
    genomic_for_heading = params[:genomic_for_heading]
    clinical_feature = params[:clinical_feature] 
    
    genomic ||= hi_lev_genomic # eliminate redundancy, in case genomic and hi_lev_genomic are both "Integrative". Get file with _Integrative_ in name (rather than the genomic or IntegrativeGenomic?)
        
    outcome_table_array = OutcomeSummary.get_outcome_table(cancer_type, data_set, gene_set, mir_set, genomic, clinical_feature)
     
    locals = {cancer_type: cancer_type, data_set: data_set, gene_set: gene_set, mir_set: mir_set, genomic: genomic, hi_lev_genomic: hi_lev_genomic, 
              clinical_feature: clinical_feature, genomic_for_heading: genomic_for_heading, outcome_table_array: outcome_table_array}   
    genomic_headers = ''
    if (hi_lev_genomic == "Integrative")
      if params[:mir_set]
        genomic_headers = %w[CopyNumber miR-Seq] 
      else
        genomic_headers = HighLevelSummary.get_genomic_headers(cancer_type, data_set, gene_set, hi_lev_genomic)
      end
      locals = locals.merge({genomic_headers: genomic_headers})
    end

    #if (outcome_table_array.blank? || outcome_table_array.size <= 1) #no file or no data in file
    if outcome_table_array.blank?
      if (hi_lev_genomic == 'Integrative Genomic')
        locals[:hi_lev_genomic] = 'Integrative'
        locals.delete(:genomic) #don't need this param to go back to Integrative Results Summary      
        respond_to do |format|
          format.html { 
            flash[:notice] = "No associated genes were found correlating #{genomic.gsub('Integrative', '')} to #{clinical_feature}"
            #flash[:notice] << " Debug::hi lev genomic=Integrative Genomic, filename=#{[cancer_type, data_set, gene_set, genomic, clinical_feature].join('_')}"
            redirect_to show_outcome_path(locals)
          }
        end # /respond_to        

      elsif ((hi_lev_genomic == 'Integrative') || (hi_lev_genomic == 'Individual'))
        locals = {cancer_type: cancer_type, data_set: data_set, gene_set: gene_set, hi_lev_genomic: hi_lev_genomic}
        respond_to do |format|
          format.html { 
            #flash[:notice] = "No associated genes were found correlating #{genomic} to #{clinical_feature}"
            flash[:notice] = "No associated data was found for #{clinical_feature}"
            #flash[:notice] << " Debug::hi lev genomic=#{hi_lev_genomic}, filename: #{[cancer_type, data_set, gene_set, genomic, clinical_feature].join('_')}"
            #redirect_to show_summary_path(locals)
            redirect_to root_path
          }
        end # /respond_to
          
      end # /if hi_lev_genomic...                
    else
      render 'show_outcome_table', :locals => locals
    end #if outcome_table...
  end #show outcome table
  
  def search_outcome_tables
           
    cancer_type = params[:cancer_type]
    data_set = params[:data_set]
    gene_set = params[:gene_set]
    cosmic_gene_set = params[:cosmic_gene_set]
    genomic = params[:genomic]
    hi_lev_genomic = params[:hi_lev_genomic]
    clinical_feature = params[:clinical_feature] 
                                                            #cancer_type, data_set, gene_set, genomic, clinical_feature
    outcome_table_array_clinicalstage = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', 'CancerGenes', '', 'Integrative', 'ClinicalStage')
    @headings = outcome_table_array_clinicalstage.shift
    outcome_table_array_tstatus = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', 'CancerGenes', '', 'Integrative', 'T-Status')
    outcome_table_array_tstatus.shift
    outcome_table_array_nstatus = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', 'CancerGenes', '', 'Integrative', 'N-Status')
    outcome_table_array_nstatus.shift
    outcome_table_array_msi = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', 'CancerGenes', '', 'Integrative', 'MSI-Status')
    outcome_table_array_msi.shift
    
# adding miR outcomes
    outcome_table_array_mir_clinicalstage = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', '', 'AllmiR', 'Integrative', 'ClinicalStage')
    @headings = outcome_table_array_mir_clinicalstage.shift
    outcome_table_array_mir_tstatus = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', '', 'AllmiR', 'Integrative', 'T-Status')
    outcome_table_array_mir_tstatus.shift
    outcome_table_array_mir_nstatus = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', '', 'AllmiR', 'Integrative', 'N-Status')
    outcome_table_array_mir_nstatus.shift
    outcome_table_array_mir_msi = OutcomeSummary.get_outcome_table('COADREAD', '2013-01-16', '', 'AllmiR', 'Integrative', 'MSI-Status')
    outcome_table_array_mir_msi.shift
        
    @outcome_table_all_hash = { #[ "a", "b" ].concat( ["c", "d"] ) #=> [ "a", "b", "c", "d" ] 
      'ClinicalStage' => outcome_table_array_clinicalstage.concat(outcome_table_array_mir_clinicalstage),
      'T-Status' => outcome_table_array_tstatus.concat(outcome_table_array_mir_tstatus),
      'N-Status' => outcome_table_array_nstatus.concat(outcome_table_array_mir_nstatus),
      'MSI-Status' => outcome_table_array_msi.concat(outcome_table_array_mir_msi)        
    }
     
    if params[:input_search]      
      gon.search_input = select_input = get_search_genes_list       
      render 'search_outcome_tables', :locals => {select_input:select_input, data_table_id:'outcome_summary_input'}                   
    else
      render 'search_outcome_tables', :locals => {select_input:'', data_table_id:'outcome_summary_all'} 
    end  
    
  end #search outcome tables
  
  def get_search_genes_list

    genes_select_input = [params[:genesearch][:genes_select]] if !params[:genesearch][:genes_select].blank?
 
    unless params[:genesearch][:genes_text].blank?
      genes_text_input = params[:genesearch][:genes_text].gsub(/[^,\s\w-]/, '') #clean string; leave comma, space, word char, hyphen alone  
      genes_text_input = genes_text_input.split(/[,\s]/)
      genes_text_input.reject! { |g| g.empty? }
    end

    genes_list = []
    if !genes_select_input.blank? && genes_text_input.blank?
      genes_list.concat( genes_select_input ) 
    elsif !genes_text_input.blank? && genes_select_input.blank?
      genes_list.concat( genes_text_input )
    elsif !genes_select_input.blank? && !genes_text_input.blank?
      genes_list.concat(genes_select_input).concat(genes_text_input)
    end
    genes_list.join('|')
  
  end
  
  def get_gene_sample
    render text: "gene: #{params[:gene]}<br />feature: #{params[:clinical_feature]}<br />genomic: #{params[:genomic]}<br />Table load and matrix queries coming next..."
    #gene_list = Gene.where(:name => params[:gene])
    #render 'show_gene', :locals => {gene_list: gene_list}
  end

  def send_file(file_type = params[:file_type], 
                cancer_type = params[:cancer_type], 
                data_set = params[:data_set], 
                gene_set = params[:gene_set], 
                mir_set = params[:mir_set],
                genomic = params[:genomic], 
                clinical_feature = params[:clinical_feature])
    file_name = [cancer_type, data_set, gene_set, genomic, clinical_feature].join("_") + '.csv'
    file_name = [cancer_type, data_set, mir_set, genomic, clinical_feature].join("_") + '.csv' if mir_set
    table_array = OutcomeSummary.get_outcome_table(cancer_type, data_set, gene_set, mir_set, genomic, clinical_feature)
    #render text: table_array
    csv_file = ApplicationController.to_csv(file_name, table_array)
    send_data csv_file, :type => 'text/csv; charset=iso-8859-1; header=present', 
      :disposition => "attachment;filename=#{file_name}"
  end
  
end
