class PagesController < ApplicationController
  def gene_selection
    @filename = sanitize_file_name(params[:filename])
    @table_array = Pages.get_gene_selection_table(@filename)
    
    if @table_array.blank?
      respond_to do |format|
        format.html { 
            flash[:notice] = "Sorry, no file was found named #{@filename}"         
            redirect_to pages_geneselection_path
          }
        end # /respond_to
    else
      render 'geneselection_table'
    end
  end

  def send_gene_selection_file(filename = sanitize_file_name(params[:filename]))
    table_array = Pages.get_gene_selection_table(filename)
    if table_array.blank?
      flash[:error] = "Invalid filename received: #{filename}"
      redirect_to pages_geneselection_path
    else
      csv_file = ApplicationController.to_csv(filename, table_array)
      send_data csv_file, :type => 'text/csv; charset=iso-8859-1; header=present',
                :disposition => "attachment;filename=#{filename.gsub('.txt', '.csv')}"
    end
  end

  def send_zip_file(filename = sanitize_file_name(params[:filename]))
    file_path = CoadreadTcga::Application::config.data_file_root + ['COADREAD_assets', filename].join('/')
    if File.exists?(file_path)
      send_file file_path, type: 'application/zip', disposition: "attachment;filename=#{filename}", x_sendfile: true
    else
      flash[:error] = "Invalid file name received: #{filename}"
      redirect_to pages_datainformation_path
    end
  end

end
