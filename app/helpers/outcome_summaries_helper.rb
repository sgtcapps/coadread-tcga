module OutcomeSummariesHelper
  
  def to_arrow(field)
    if field.to_s.match(/\+/) 
       newfield = field.gsub(/(\+)\Z/,' &uarr;')
    elsif field.to_s.match(/\-/) 
       newfield = field.gsub(/(\-)\Z/,' &darr;')     
    end
  end
end
