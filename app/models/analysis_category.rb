class AnalysisCategory < ApplicationRecord
  
  def self.get_feature_samples_table(fn)
    file_path = CoadreadTcga::Application::config.data_file_root + ['2013-01-16', 'COADREAD', 'Matrix', fn].join('/')
    feature_samples_table_array = ApplicationController.get_table_array(file_path) if File.exists?(file_path)
    feature_samples_table_array
  end
  
  def self.get_feature_samples_metadata_table(fn)
    file_path = CoadreadTcga::Application::config.data_file_root + ['2013-01-16', 'COADREAD', 'HighLevel_Summary', fn].join("/")
    feature_samples_metadata_table_array = ApplicationController.get_table_array(file_path) if File.exists?(file_path)
    feature_samples_metadata_table_array
  end
  
end
