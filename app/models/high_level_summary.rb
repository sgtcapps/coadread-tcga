class HighLevelSummary < ApplicationRecord

  def self.get_summary_table(cancer_type, data_set, gene_set, hi_lev_genomic)
    summary_file_name = [cancer_type, data_set, gene_set, hi_lev_genomic].join("_") + ".txt"                  
    summary_file_path = CoadreadTcga::Application::config.data_file_root + [data_set, cancer_type, 'HighLevel_Summary', summary_file_name].join("/")
    summary_table_array = ApplicationController.get_table_array(summary_file_path) if File.exists?(summary_file_path)    
  end
  
  def self.get_genomic_headers(cancer_type, data_set, gene_set, hi_lev_genomic)
    summary_file_name = [cancer_type, data_set, gene_set, hi_lev_genomic].join('_') + '.txt'           
    summary_file_path = CoadreadTcga::Application::config.data_file_root + [data_set, cancer_type, 'HighLevel_Summary', summary_file_name].join("/")
    
    summary_table_array = ApplicationController.get_table_array(summary_file_path) if File.exists?(summary_file_path)    
    headers = summary_table_array.shift
    headers = headers[3].split('|')
    headers = headers[1].split(',')
  end
  
end
