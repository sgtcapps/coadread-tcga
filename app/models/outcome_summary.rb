class OutcomeSummary < ApplicationRecord
        
  def self.get_outcome_table(cancer_type, data_set, gene_set, mir_set, genomic, clinical_feature) 
    if !mir_set.blank?
      outcome_file_name = [cancer_type, data_set, mir_set, genomic, clinical_feature].join("_") + ".txt"       
      outcome_file_path = CoadreadTcga::Application::config.data_file_root + [data_set, cancer_type, 'Outcome_Summary_miR', outcome_file_name].join("/") 
    else
      outcome_file_name = [cancer_type, data_set, gene_set, genomic, clinical_feature].join("_") + ".txt"       
      outcome_file_path = CoadreadTcga::Application::config.data_file_root + [data_set, cancer_type, 'Outcome_Summary', outcome_file_name].join("/")             
    end    
    outcome_table_array = ApplicationController.get_table_array(outcome_file_path) if File.exists?(outcome_file_path)    
  end
  
end
