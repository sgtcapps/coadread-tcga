class Pages < ApplicationRecord
    
  def self.get_gene_selection_table(fn)
    file_path = CoadreadTcga::Application::config.data_file_root + ['COADREAD_assets', 'Gene_List', fn].join('/')
    gene_selection_table_array = ApplicationController.get_table_array(file_path) if File.exists?(file_path)
    gene_selection_table_array
  end
  
end
