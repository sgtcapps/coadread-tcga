Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # analysis_categories
  root :to => 'analysis_categories#select_categories', via: [:get, :post]
  match 'get_feature_samples', to: 'analysis_categories#get_feature_samples', via: [:get, :post]
  match 'feature_samples_sendfile', to: 'analysis_categories#send_feature_samples_file', via: [:get, :post]

  # pages
  match "pages/geneselection", via: 'get'
  match 'gene_selection', to: 'pages#gene_selection', via: [:get, :post]
  match 'gene_selection_sendfile', to: 'pages#send_gene_selection_file', via: [:get, :post]

  match "pages/analysispipeline", to: 'pages#analysispipeline', via: 'get'
  match "pages/datainformation", to: 'pages#datainformation', via: 'get'
  match "pages/projectoverview", to: 'pages#projectoverview', via: 'get'
  match "pages/interpretation", to: 'pages#interpretation', via: 'get'
  match "pages/about", to: 'pages#about', via: 'get'

  match 'send_zip_file', to: 'pages#send_zip_file', via: [:get, :post]

  # high_level_summaries
  match 'show_summary', to: 'high_level_summaries#show_summary_table',  via: :get

  # outcome_summaries
  match 'show_outcome', to: 'outcome_summaries#show_outcome_table',  via: [:get, :post]
  match 'send_file', to: 'outcome_summaries#send_file', via: [:get, :post]
  match 'search_all_outcome_tables', to: 'outcome_summaries#search_outcome_tables', via: [:get, :post]
  match 'search_input_outcome_tables', to: 'outcome_summaries#search_outcome_tables', via: [:get, :post]
  match 'search_genes', to: 'outcome_summaries#search_genes', via: [:get, :post]

end
