# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

=begin
1. Data - 11302012cBio (only choice at this point)
2. Tissue - COADREAD (only choice at this point)
3. Gene - 484, Extend
4. Genomics - Allsets, CNLog2, ExpzArray, ExpzRNAseq, GISTIC, Methylation, Mutations
5. ClinicalFeature - anatomicorgansubdivision, daystodeath, distantmetastasispathologicspread, hypermutated, lymphaticinvasionpresent, 
lymphnodepathologicspread, MSIstatus, 
numberoffirstdegreerelativeswithcancerdiagnosis, primarytumorpathologicspread, tumorsite,tumorstage, vascularinvasionpresent, vitalstatus
File format: Data_Tissue_Gene_Sample_Genomics_ClinicalFeature.txt 
Example: 11302012cBio_COADREAD_484_complete180_Allsets_tumorstage.txt
=end

ctype = ClinicalFeature.find(5)
ctype.samples.build(:name => "sample01")
ctype.samples.build(:name => "sample03")
ctype.samples.build(:name => "sample04")
ctype.save

ctype = ClinicalFeature.find(6)
ctype.samples.build(:name => "this_sample")
ctype.save

ctype = ClinicalFeature.find(7)
ctype.samples.build(:name => "another_sample")
ctype.save

ctype = ClinicalFeature.find(9)
ctype.samples.build(:name => "sample01")
ctype.samples.build(:name => "sample03")
ctype.samples.build(:name => "sample04")
ctype.save

ctype = ClinicalFeature.find(10)
ctype.samples.build(:name => "this_sample")
ctype.save

ctype = ClinicalFeature.find(11)
ctype.samples.build(:name => "another_sample")
ctype.save

ctype = ClinicalFeature.find(12)
ctype.samples.build(:name => "sample012")
ctype.samples.build(:name => "sample013")
ctype.samples.build(:name => "sample014")
ctype.save

ctype = ClinicalFeature.find(13)
ctype.samples.build(:name => "this_sample13")
ctype.save

ctype = ClinicalFeature.find(14)
ctype.samples.build(:name => "another_sample14")
ctype.save

ctype = ClinicalFeature.find(15)
ctype.samples.build(:name => "this_samplez")
ctype.samples.build(:name => "this_samplex")
ctype.save

ctype = ClinicalFeature.find(16)
ctype.samples.build(:name => "another_samplez")
ctype.samples.build(:name => "this_samplea")
ctype.samples.build(:name => "this_sampleb")
ctype.samples.build(:name => "this_samplec")
ctype.save
=begin
categ5.analysis_category_values.build(:cat_val => "primarytumorpathologicspread")
categ5.analysis_category_values.build(:cat_val => "tumorsite")
categ5.analysis_category_values.build(:cat_val => "tumorstage")
categ5.analysis_category_values.build(:cat_val => "vascularinvasionpresent")
categ5.analysis_category_values.build(:cat_val => "vitalstatus")
categ5.save
=end
