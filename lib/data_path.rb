module DataPath
  def get_path
    data_file_root = '/mnt/tcgaweb/' if (Rails.env == "production")
  rescue NoMethodError
    config.data_file_root = '/Volumes/tcgaweb/'
  end
end