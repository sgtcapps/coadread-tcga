require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get geneselection" do
    get :geneselection
    assert_response :success
  end

  test "should get analysispipeline" do
    get :analysispipeline
    assert_response :success
  end

  test "should get clinicalinformation" do
    get :clinicalinformation
    assert_response :success
  end

end
