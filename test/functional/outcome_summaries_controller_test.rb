require 'test_helper'

class OutcomeSummariesControllerTest < ActionController::TestCase
  setup do
    @outcome_summary = outcome_summaries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:outcome_summaries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create outcome_summary" do
    assert_difference('OutcomeSummary.count') do
      post :create, outcome_summary: {  }
    end

    assert_redirected_to outcome_summary_path(assigns(:outcome_summary))
  end

  test "should show outcome_summary" do
    get :show, id: @outcome_summary
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @outcome_summary
    assert_response :success
  end

  test "should update outcome_summary" do
    put :update, id: @outcome_summary, outcome_summary: {  }
    assert_redirected_to outcome_summary_path(assigns(:outcome_summary))
  end

  test "should destroy outcome_summary" do
    assert_difference('OutcomeSummary.count', -1) do
      delete :destroy, id: @outcome_summary
    end

    assert_redirected_to outcome_summaries_path
  end
end
